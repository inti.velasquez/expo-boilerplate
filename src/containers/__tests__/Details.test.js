import React from "react";
import renderer from "react-test-renderer";

import Details from "../Details";

describe("<Details />", () => {
  it("has 2 children", () => {
    const tree = renderer.create(<Details />).toJSON();
    expect(tree.children.length).toBe(2);
  });
});
