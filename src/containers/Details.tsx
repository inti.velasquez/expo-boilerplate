import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';

const Details = ({ navigation }: { navigation: any }) => {
  return (
    <View style={styles.container}>
      <Text>Modify screen!</Text>
      <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Details;
