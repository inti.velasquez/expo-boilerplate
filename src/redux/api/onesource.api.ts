import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const onesourceApi = createApi({
  reducerPath: "onesourceApi",
  baseQuery: fetchBaseQuery({ baseUrl: "" }),
  tagTypes: ["Test"],
  endpoints: () => ({}),
});
