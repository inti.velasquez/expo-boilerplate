import { onesourceApi } from "./onesource.api";

const userEndpoints = onesourceApi.injectEndpoints({
  endpoints: (build) => ({
    getTest: build.query({
      query: () => "/test",
      providesTags: ["Test"],
    }),
  }),
});

export const { useGetTestQuery } = userEndpoints;
