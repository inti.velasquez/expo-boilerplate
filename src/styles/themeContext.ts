import { createContext } from "react";
import { globalStyles } from "./styles";

export const ThemeContext = createContext(globalStyles);
